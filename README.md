# Git Ignorance
This repository contains any necessary ignorance files that you should use when using Git 
for a project. The primary file is the .gitignore_global which should be configured on your 
machine. This has been configured in a specific way to keep consistency between each 
developer's projects.

To add a global ignorance file to your configuration, you need to create it somewhere on 
your system (preferably under the primary user directory). On Windows this would be 
`C:\Users\{user}` and on OS X this would be `~`. The command you need to run, which won't 
need to be run again, is as follows:

```
git config --global core.excludesfile "YOUR_DIRECTORY"
```

Bear in mind that the file needs to exist wherever you've specified it in the command (see 
examples below) and contain the necessary contents from the .gitignore_global file in this 
repository.

## Examples

#### Windows
Creating a globalised ignorance file for Bob in a Windows environment:

```
git config --global core.excludesfile "C:\Users\Bob\.gitignore"
```

#### OS X
Creating a globalised ignorance file for Jane in a UNIX/Darwin environment:

```
git config --global core.excludesfile "~/.gitignore"
```

## Adding/removing objects from .gitignore_global
If you've noticed something in the .gitignore_global file that's causing issues with you 
utilising Git properly, then please submit a ticket in the [Issues](https://bitbucket.org/gooeycreative/git-ignorance/issues) section so 
that we're aware it needs to be removed.

In the meantime, if you create a localised igoranice in your repository until we can make 
the change, then this should resolve your issue temporarily.